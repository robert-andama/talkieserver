# Talkie Server

##### Personal message
- A user should be able to send a personal message to another user
##### Live chat
- Messages should automatically be updated without having to refresh the page.
##### SignUp and LogIn
- JWT-based authentication is required. 
- Implementation is done with with laravel passport
##### API
- A deployed API. 
- API is build with Laravel
##### Model-View-Presenter Pattern
- The app should use the MVP structure. 
- A clean project structure and app architecture is
a major factor in the assessment of the app. 
- It also utilizes the following libraries:
RxJava​, ​Retrofit​, and ​Room​.
