<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Chats extends Model
{
    /*
     * Fields to mass assign
     *
     */
    protected $fillable = ['from', 'to', 'message', 'is_read'];

//    protected $attributes = ['messages'];

    /**
    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setTimeAgoAttribute($value)
    {
        $this->attributes['time_ago'] = $value->diffForHumans(null, null, true);;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "to");
    }

    /**
     * Mark all unread message as read
     *
     * @param $recipient_user
     * @param $auth_user
     * @return mixed
     */
    public static function updateChatStatus($recipient_user, $auth_user)
    {
        $recipient_id = $recipient_user->id;
        $auth_id = $auth_user->id;

        return static::where(
            [
                'from' => $recipient_id,
                'to' => $auth_id
            ])
            ->update(['is_read' => 1]);
    }

    /**
     * Get all message from selected recipient_user
     *
     * @param $recipient_user
     * @param $auth_user
     * @return mixed
     */
    public static function allChatConversations($recipient_user, $auth_user)
    {
        $recipient_id = $recipient_user->id;
        $auth_id = $auth_user->id;
        return static::where(function ($query) use ($recipient_id, $auth_id) {
            $query->where('from', $recipient_id)
                ->where('to', $auth_id);
        })
            ->oRwhere(function ($query) use ($recipient_id, $auth_id) {
                $query->where('from', $auth_id)
                    ->where('to', $recipient_id);
            })
            ->get();
    }


    /**
     * @param $user
     * @param $message
     * @return mixed
     */
    public static function saveChatMessage($user, $message)
    {
        return static::create([
            'from' => Auth::id(),
            'to' => $user->id,
            'message' => $message
        ]);
    }
}
