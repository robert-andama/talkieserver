<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ChatMessageWasReceived implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $chatMessage;
    public $user;

    /**
     * Create a new event instance.
     * @param $chatMessage
     * @param $user
     */
    public function __construct($chatMessage, $user)
    {
        $this->chatMessage = $chatMessage;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     * For this case Private channel is used
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        $collection = collect([Auth::id(), $this->user->id]);
        $vars = $collection->sort()->values();
        return new  Channel("talkie-new-message.$vars[0]$vars[1]");
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'new-event-message';
    }

}
