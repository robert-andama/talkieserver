<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TalkiePresenceChannel implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('talkie-room.' . $this->user->id);
    }

    /**
     * Get the data to broadcast.
     *
     * @return User|array
     */
    public function broadcastWith()
    {
        $id = $this->user->id;
        return [
            "user_id" => (String)$id,
            "user_info" => [
                'id' => (String)$id,
                'name' => $this->user->name,
                'created_at' => $this->user->created_at->toDateTimeString(),
                'email' => $this->user->email,
                'email_verified_at' => !$this->user->email_verified_at ?: $this->user->email_verified_at->toDateTimeString(),
                'is_read_count' => $this->user->is_read_count,
                'messages_count' => $this->user->messages_count,
                'un_read_count' => $this->user->un_read_count,
                'updated_at' => $this->user->updated_at->toDateTimeString()
            ]
        ];
    }


    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'presence-event';
    }
}
