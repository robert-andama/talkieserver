<?php

namespace App\Http\Controllers\Api\Auth;

use App\Chats;
use App\Events\ChatMessageWasReceived;
use App\Events\TalkiePresenceChannel;
use App\Events\TalkiePrivateChannel;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withChatDetails();

        if (\request()->wantsJson())
            return $users;

        return $users;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        // Validation
        $validatorResponse = $this->validateRequest($request, ['message' => "required"]);
        // Send failed response if validation fails
        if ($validatorResponse !== true)
            return $this->sendInvalidFieldResponse($validatorResponse);

        $message = $request->message;
        Chats::saveChatMessage($user, $message);

        $chats = Chats::all()->last();
        $chats->time_ago = $chats->created_at;

        // Fire off the ChatMessageWasReceived event
        event(new ChatMessageWasReceived($chats, $user));

        return $chats;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chats $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // load  recipient_user plus chats associated
        $recipient_user = $user->load("messages");

        // authenticated user
        $auth_user = Auth::user();

        Chats::updateChatStatus($recipient_user, $auth_user);

        $chats = Chats::allChatConversations($recipient_user, $auth_user);

        // add time ago
        $chats->each(function ($item) {
            $item->time_ago = $item['created_at'];
        });

        if (\request()->wantsJson())
            return $chats;

        return $chats;
    }

    /**
     * Authenticate user against the Presence channel
     * @param Request $request
     * @return string
     */
    public function presence(Request $request)
    {
        $pusher = new Pusher(
            config('broadcasting.connections.pusher.key'),
            config('broadcasting.connections.pusher.secret'),
            config('broadcasting.connections.pusher.app_id'),
            config('broadcasting.connections.pusher.options')
        );

        $user = [
            "user_id" => (String)Auth::id(),
            "user_info" => [
                'id' => (String)Auth::id(),
                'name' => Auth::user()->name,
                'created_at' => Auth::user()->created_at->toDateTimeString(),
                'email' => Auth::user()->email,
                'email_verified_at' => !Auth::user()->email_verified_at ?: Auth::user()->email_verified_at->toDateTimeString(),
                'is_read_count' => Auth::user()->is_read_count?:0,
                'messages_count' => Auth::user()->messages_count?:0,
                'un_read_count' => Auth::user()->un_read_count?:0,
                'updated_at' => Auth::user()->updated_at->toDateTimeString()
            ]
        ];

        return $pusher->presence_auth($request->channel_name, $request->socket_id, (String)Auth::id(),$user);

    }

    /**
     * Authenticate user against the Presence channel
     */
    public function private ()
    {
        event(new TalkiePrivateChannel(Auth::user()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chats $chats
     * @return \Illuminate\Http\Response
     */
    public function edit(Chats $chats)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Chats $chats
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chats $chats)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chats $chats
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chats $chats)
    {
        //
    }
}
