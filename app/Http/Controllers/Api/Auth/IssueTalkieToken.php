<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 3/11/2020
 * Time: 5:44 PM
 */
trait IssueTalkieToken
{

    /**
     * @param Request $request
     * @param $grantType
     * @param string $scope
     * @return mixed
     */
    public function IssueToken(Request $request, $grantType, $scope = "")
    {

        $form_data = [
            'grant_type' => $grantType,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->username ?: $request->email,
            'password' => $request->password,
            'scope' => $scope,
        ];

        $request->request->add($form_data);
        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }
}