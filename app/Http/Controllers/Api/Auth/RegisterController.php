<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client;

class RegisterController extends Controller
{

    use IssueTalkieToken;

    /**
     * The attribute that holds the client (Talkie)
     *
     * @var string
     */
    private $client;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->client = Client::find(6);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function register(Request $request)
    {

        // validate request
        $validatorResponse = $this->validator($request);
        // Send failed response if validation fails
        if ($validatorResponse !== true)
            return $this->sendInvalidFieldResponse($validatorResponse);
        // Fire new user register event
        event(new Registered($user = $this->create($request->all())));
        // issue token
        return $this->issueToken($request, 'password');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array|Request $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $data)
    {
        return $this->validateRequest(
            $data,
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
