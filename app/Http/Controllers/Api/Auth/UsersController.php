<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Get all users
     *
     * @return mixed
     */
    public function index()
    {
      return  \request()->user();
    }
}
