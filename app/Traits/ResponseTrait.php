<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 3/11/2020
 * Time: 7:12 PM
 */
trait ResponseTrait
{

    /**
     * Status code of response
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Fractal manager instance
     *
     * @var Manager
     */
    protected $fractal;

    /**
     * Set fractal Manager instance
     *
     * @param Manager $fractal
     * @return void
     */
    public function setFractal(Manager $fractal)
    {
        $this->fractal = $fractal;
    }

    /**
     * Getter for statusCode
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Send custom data response
     *
     * @param $status
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendCustomResponse($status, $message)
    {
        return response()->json(['status' => $status, 'message' => $message], $status);
    }

    /**
     * Send this response when api user provide fields that doesn't exist in our application
     *
     * @param $errors
     * @return mixed
     */
    public function sendUnknownFieldResponse($errors)
    {
        $str = implode(", ", $errors);
        Log::notice("status: 400 : message: {$str}");
        return response()->json((['status' => 400, 'unknown_fields' => $errors]), 400);
    }

    /**
     * Send this response when api user provide filter that doesn't exist in our application
     *
     * @param $errors
     * @return mixed
     */
    public function sendInvalidFilterResponse($errors)
    {
        $str = implode(", ", $errors);
        Log::notice("status: 400 : message: {$str}");
        return response()->json((['status' => 400, 'invalid_filters' => $errors]), 400);
    }

    /**
     * Send this response when api user provide incorrect data type for the field
     *
     * @param $errors
     * @return mixed
     */
    public function sendInvalidFieldResponse($errors)
    {
        $str = implode(", ", $errors);
        Log::notice("status: 422 : message: {$str}");
        return response()->json((['status' => 422, 'invalid_fields' => $errors]), 400);
    }

    /**
     * Send this response when a api user try access a resource that they don't belong
     *
     * @return string
     */
    public function sendForbiddenResponse()
    {
        Log::notice("status: 403 : message: Forbidden");
        return response()->json(['status' => 403, 'message' => 'Forbidden'], 403);
    }

    /**
     * Send 404 not found response
     *
     * @param string $message
     * @return string
     */
    public function sendNotFoundResponse($message = '')
    {
        if ($message === '') {
            $message = 'The requested resource was not found';
        }

        Log::notice("status: 404 : message: {$message}");
        return response()->json(['status' => 404, 'message' => $message, "documentation_url" => url("docs")], 404);
    }

    /**
     * Send empty data response
     *
     * @return string
     */
    public function sendEmptyDataResponse()
    {
        Log::notice("sent empty data");
        return response()->json(['data' => new \StdClass()]);
    }


    /**
     * Return a json response from the application
     *
     * @param array $array
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithArray(array $array, array $headers = [])
    {
        return response()->json($array, $this->statusCode, $headers);
    }

    /**
     * @param $path
     * @param $photo
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    protected function respondWithAvatar($path, $photo)
    {
        $path = !File::exists($path) ? storage_path("images/antsms_logo.png") : $path;
        $type = File::mimeType($path);
        $headers = ["Content-Type" => $type];

        return response()->download($path, $photo, $headers);
    }

    public function sendProviderNotSupportedResponse($provider = '')
    {
        $message = $provider === '' ? 'No provider supplied' : "{$provider} is not currently supported";
        return response()->json(['status' => 404, 'message' => $message, "documentation_url" => url("docs/auth/social")], 404);
    }

}