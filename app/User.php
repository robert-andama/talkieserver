<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Chats::class, "from");
    }


    /**
     * @return mixed
     */
    public static function withChatDetails()
    {
        return static::withCount(['messages as un_read_count' => function ($message) {
            return $message
                ->where('is_read', 0)
                ->where('to', Auth::id());
        }])
            ->withCount(['messages as is_read_count' => function ($message) {
                return $message
                    ->where('is_read', 1)
                    ->where('to', Auth::id());
            }])
            ->withCount(['messages' => function ($message) {
                return $message
                    ->where('to', Auth::id());
            }])
            ->get();
    }

}
