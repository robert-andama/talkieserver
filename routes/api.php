<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// login
Route::middleware('auth:api')->get('/users', "Api\Auth\UsersController@index");

// sign up
Route::post('register', 'Api\Auth\RegisterController@register');

// chats
Route::middleware('auth:api')->get('chats', 'Api\Auth\ChatController@index');
Route::middleware('auth:api')->get('chats/{user}', 'Api\Auth\ChatController@show');
Route::middleware('auth:api')->post('send-chat/{user}', 'Api\Auth\ChatController@store');

// channels
Route::middleware('auth:api')->post('/pusher/auth/presence', 'Api\Auth\ChatController@presence');
Route::middleware('auth:api')->post('/pusher/auth/private', 'Api\Auth\ChatController@private');
