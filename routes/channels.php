<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int)$user->id === (int)$id;
});


Broadcast::channel('talkie-room.{userId}', function ($user, $userId) {
    if ( (int)$user->id ===  (int)$userId) {
        return [
            "user_id" => $user->id,
            "user_info" => [
                'id' => $user->id,
                'name' => $user->name,
                'created_at' => $user->created_at,
                'email' => $user->email,
                'email_verified_at' => $user->email_verified_at,
                'is_read_count' => $user->is_read_count,
                'messages_count' => $user->messages_count,
                'un_read_count' => $user->un_read_count,
                'updated_at' => $user->updated_at
            ]
        ];
    }
});
